#ifndef DB_TEST_H
#define DB_TEST_H

#include <QtSql/QSqlDatabase>
#include <QString>

class DB_Test {
private:
    QSqlDatabase _db;
public:
    explicit DB_Test();
    virtual ~DB_Test();
    void connectDB(const QString &dbname);
    void disconnectDB();
    void selectQuery();
    void insertQuery();
    void updateQuery();
    void deleteQuery();

    void getTables();
    void getTypes();
};

#endif // DB_TEST_H
