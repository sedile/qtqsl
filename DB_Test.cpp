#include "DB_Test.h"

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QDebug>

DB_Test::DB_Test(){}

DB_Test::~DB_Test(){}

void DB_Test::connectDB(const QString &dbname){
    if ( dbname.isNull() || dbname.isEmpty() ){
        qDebug() << "Invalid Databasename";
        return;
    }

    _db = QSqlDatabase::addDatabase("QSQLITE");
    _db.setDatabaseName(dbname);
    _db.open();

    if ( !_db.isOpen() ){
        qDebug() << "Database could not be opened";
        return;
    }

    qDebug() << "Offen";
}

void DB_Test::disconnectDB(){
    _db.close();
}

void DB_Test::selectQuery(){
    QSqlQuery select;
    select.exec("SELECT branch_id, branch_name FROM Branch WHERE mgr_id <= 102;");

    while( select.next() ){
        QString id = select.record().value(0).toString();
        QString name = select.record().value(1).toString();
        qDebug() << id << name;

        /* Alles ausgeben
        for(int i = 0; i < select.record().count(); i++){
            row = row + " " + select.record().value(i).toString();
        }
        qDebug() << row;
        */
    }
}

void DB_Test::insertQuery(){
    QSqlQuery insert;
    insert.prepare("INSERT INTO Branch(branch_id, branch_name,mgr_id,mgr_start_date) VALUES(?,?,?,?)");
    insert.addBindValue(666);
    insert.addBindValue("Vault-Tec");
    insert.addBindValue(100);
    insert.addBindValue("2077-03-01");
    insert.exec();
}

void DB_Test::updateQuery(){
    QSqlQuery update;
    update.exec("UPDATE Branch SET branch_name='Vault-Tec Corp.' WHERE branch_id=666");
}

void DB_Test::deleteQuery(){
    QSqlQuery remove;
    remove.exec("DELETE FROM Branch WHERE branch_id=666");
}

void DB_Test::getTables(){
    QStringList tables = _db.tables();
    for(QString tablename : tables){
        qDebug() << tablename;
    }
}

void DB_Test::getTypes(){
    QSqlRecord tuple = _db.record("employee");

    for(int i = 0; i < tuple.count(); i++){
       qDebug() << tuple.field(i).type() << tuple.field(i).name();
    }
}
